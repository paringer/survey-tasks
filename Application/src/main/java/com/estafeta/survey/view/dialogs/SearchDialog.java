package com.estafeta.survey.view.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.estafeta.survey.model.UserProfile;
import com.estafeta.survey.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Component;

/**
 * Created by Zhenya on 29.11.2016.
 */
@Component
public class SearchDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = SearchDialog.class.getName();

    @Bind(R.id.searchEditText)
    EditText vSearch;
    @Bind(R.id.searchType)
    Spinner vSearchType;
    @Bind(R.id.imageButton)
    View vButton;

    OnPositiveListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater(savedInstanceState).inflate(R.layout.search_dialog, container);
        ButterKnife.bind(this, view);
        getDialog().setTitle("Search");
        getDialog().setTitle(R.string.search);
        vButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        UserProfile profile = UserProfile.getInstance(getActivity());
        String search = null;
        int searchType = -1;
        if(vSearch != null && profile != null) profile.setCurrentSearch(search = vSearch.getText().toString());
        if(vSearchType!=null && profile != null) profile.setSearchType(searchType = vSearchType.getSelectedItemPosition());
        if(mListener != null) mListener.onPositiveButtonClick(getDialog(), vButton, search, searchType);
        dismiss();
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    public void setOnPositiveListener(OnPositiveListener listener){mListener = listener;}
    public interface OnPositiveListener {
        void onPositiveButtonClick(Dialog dialog, View button, String search, int searchType);
    }
}
