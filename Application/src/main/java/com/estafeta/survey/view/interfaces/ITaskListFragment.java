package com.estafeta.survey.view.interfaces;

import android.support.v4.app.FragmentActivity;

import com.estafeta.survey.presenter.common.adapters.TaskItemAdapter;
import com.estafeta.survey.view.TaskListFragment;

/**
 * Created by Zhenya on 29.11.2016.
 */
public interface ITaskListFragment {
    void setRecyclerViewLayoutManager(TaskListFragment.LayoutManagerType layoutManagerType);
    void back();

    void setAdapter(TaskItemAdapter taskItemAdapter);

    FragmentActivity getActivity();
}
