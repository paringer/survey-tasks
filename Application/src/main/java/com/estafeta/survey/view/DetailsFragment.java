package com.estafeta.survey.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estafeta.survey.model.SurveyTask;
import com.estafeta.survey.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SURVEY_TASK = "surveyTask";
    public static final String LOG_TAG = DetailsFragment.class.getName();

    private SurveyTask mSurveyTask;

    @Bind(R.id.brandValue)
    TextView vBrandValue;
    @Bind(R.id.modelValue)
    TextView vModelValue;
    @Bind(R.id.carrierValue)
    TextView vCarrierValue;
    @Bind(R.id.driverValue)
    TextView vDriverValue;
    @Bind(R.id.surveyPointValue)
    TextView vSurveyPointValue;
    @Bind(R.id.dateStartValue)
    TextView vDateStartValue;
    @Bind(R.id.dateEndValue)
    TextView vDateEndValue;
    @Bind(R.id.plannedDateStartValue)
    TextView vPlannedDateStartValue;
    @Bind(R.id.plannedDateEndValue)
    TextView vPlannedDateEndValue;
    @Bind(R.id.numberValue)
    TextView vNumberValue;
    @Bind(R.id.vinValue)
    TextView vVinValue;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyTask SurveyTask
     * @return A new instance of fragment DetailsFragment.
     */
    public static DetailsFragment newInstance(SurveyTask surveyTask) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SURVEY_TASK, surveyTask);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSurveyTask = getArguments().getParcelable(ARG_SURVEY_TASK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_fragment, container, false);
        ButterKnife.bind(this, view);
        bindTask(mSurveyTask);
        return view;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    void bindTask(SurveyTask surveyTask){
        vBrandValue.setText(surveyTask.Brand);
        vModelValue.setText(surveyTask.Model);
        vCarrierValue.setText(surveyTask.Carrier);
        vDriverValue.setText(surveyTask.Driver);
        vSurveyPointValue.setText(surveyTask.SurveyPoint);
        vDateStartValue.setText(surveyTask.ActualStartDate);
        vDateEndValue.setText(surveyTask.ActualEndDate);
        vPlannedDateStartValue.setText(surveyTask.PlannedStartDate);
        vPlannedDateEndValue.setText(surveyTask.PlannedEndDate);
        vNumberValue.setText(surveyTask.Number);
        vVinValue.setText(surveyTask.Vin);
    }
}
