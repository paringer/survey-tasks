package com.estafeta.survey.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.estafeta.survey.model.UserProfile;
import com.estafeta.survey.presenter.common.interfaces.IAuthorizable;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Component;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@Component
public class LoginFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_USERNAME = "username";
    public static final String LOG_TAG = LoginFragment.class.getName();

    private IAuthorizable mListener;

//    Unbinder unbinder;
    @Bind(R.id.usernameValue)
    EditText vUserName;
    @Bind(R.id.passwordValue)
    EditText vPassword;
    @Bind(R.id.rememberMeValue)
    CheckBox vRememberMe;
    @Bind(R.id.signInButton)
    View vSignInButton;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_dialog, container, false);
        ButterKnife.bind(this, view);
        if(vSignInButton != null) vSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginFragment.this.onClick(v);
            }
        });
        UserProfile profile = UserProfile.getInstance(getActivity());
        if(profile!=null && profile.isRememberMe()){
            if (vUserName != null) vUserName.setText(profile.getUsername());
        }
        if(profile!=null && vRememberMe != null) vRememberMe.setChecked(profile.isRememberMe());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IAuthorizable) {
            mListener = (IAuthorizable) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(LOG_TAG, "MYBUTTON");
        if (mListener instanceof IAuthorizable) {
            mListener.authorize(String.valueOf(vUserName.getText()), String.valueOf(vPassword.getText()), vRememberMe.isChecked());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener extends IAuthorizable{
        void onFragmentInteraction(Uri uri);
    }
}
