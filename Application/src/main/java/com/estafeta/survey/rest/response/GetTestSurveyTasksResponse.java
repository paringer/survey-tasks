package com.estafeta.survey.rest.response;

import com.estafeta.survey.model.SurveyTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhenya on 27.11.2016.
 */
public class GetTestSurveyTasksResponse extends ArrayList<SurveyTask> {
    public GetTestSurveyTasksResponse() {
        super();
    }
}
