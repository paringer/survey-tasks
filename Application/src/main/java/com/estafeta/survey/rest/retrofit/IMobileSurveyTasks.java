package com.estafeta.survey.rest.retrofit;

import com.estafeta.survey.rest.request.GetTestSurveyTasksRequest;
import com.estafeta.survey.rest.response.GetTestSurveyTasksResponse;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;

/**
 * Created by Zhenya on 27.11.2016.
 */
public interface IMobileSurveyTasks {
    @Headers({
            "Host: amt1.estafeta.org",
            "Connection: keep-alive",
            "Cache-Control: max-age=0",
//            "Authorization: Basic ZXN0YWZldGE6MQ==",
//            "Upgrade-Insecure-Requests: 1",
//            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
//            "Accept-Encoding: gzip, deflate, sdch",
            "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
//            "Cookie: ClientUiCulture=en; ianaTimeZoneId=Europe%2FKiev; ASP.NET_SessionId=mnrlqrag1m20dexuamjfqwsl"
    })
    @GET("/api/mobilesurveytasks/gettestsurveytasks")
    Call<GetTestSurveyTasksResponse> get(@Header("Authorization") String authorization);//
//    Call<GetTestSurveyTasksResponse> get(@Body GetTestSurveyTasksRequest request, Callback<GetTestSurveyTasksResponse> callback);
}
