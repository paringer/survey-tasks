package com.estafeta.survey.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zhenya on 27.11.2016.
 */
public class SurveyTask implements Parcelable{
    public int Id;
    public String Number;
    public String PlannedStartDate;
    public String PlannedEndDate;
    public String ActualStartDate;
    public String ActualEndDate;
    public String Vin;
    public String Model;
    public String ModelCode;
    public String Brand;
    public String SurveyPoint;
    public String Carrier;
    public String Driver;

    public SurveyTask() {
    }

    public SurveyTask(Parcel in) {
        Id = in.readInt();
        Number = in.readString();
        PlannedStartDate = in.readString();
        PlannedEndDate = in.readString();
        ActualStartDate = in.readString();
        ActualEndDate = in.readString();
        Vin = in.readString();
        Model = in.readString();
        ModelCode = in.readString();
        Brand = in.readString();
        SurveyPoint = in.readString();
        Carrier = in.readString();
        Driver = in.readString();
    }

    public static final Creator<SurveyTask> CREATOR = new Creator<SurveyTask>() {
        @Override
        public SurveyTask createFromParcel(Parcel in) {
            return new SurveyTask(in);
        }

        @Override
        public SurveyTask[] newArray(int size) {
            return new SurveyTask[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Number);
        dest.writeString(PlannedStartDate);
        dest.writeString(PlannedEndDate);
        dest.writeString(ActualStartDate);
        dest.writeString(ActualEndDate);
        dest.writeString(Vin);
        dest.writeString(Model);
        dest.writeString(ModelCode);
        dest.writeString(Brand);
        dest.writeString(SurveyPoint);
        dest.writeString(Carrier);
        dest.writeString(Driver);
    }
}
