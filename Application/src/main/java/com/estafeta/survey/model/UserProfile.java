package com.estafeta.survey.model;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.estafeta.survey.presenter.common.logger.Log;
import com.estafeta.survey.presenter.common.interfaces.IAuthorizable;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import javax.inject.Singleton;

/**
 * Created by Zhenya on 28.11.2016.
 */
@Singleton
public class UserProfile implements IAuthorizable{
    private final static String PREF = "com.estafeta.userprofile_pref";
    private final static String PREF_USERPROFILE = "com.estafeta.userprofile";
    public static final String COMPANY_ID_DEFAULT = "9F346DDB-8FF8-4F42-8221-6E03D6491756";

    private static volatile UserProfile instance;

    String username;
    boolean rememberMe = false;
    transient String password;

    transient String currentSearch;
    transient int searchType;

    public UserProfile() {
    }

    public static UserProfile getInstance(Context context) {
        if (instance == null) {
            instance = load(context);
        }
        return instance;
    }

    public void save(Activity context) {
        if (context == null)
            return;
        SharedPreferences sharedPref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String json = new Gson().toJson(this);
        Log.d("UserProfile", json);
        editor.putString(PREF_USERPROFILE, json);
        editor.apply();
    }

    private static UserProfile load(Context context) {
        if (context == null)
            return new UserProfile();
        SharedPreferences sharedPref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        String json = sharedPref.getString(PREF_USERPROFILE, null);
        if (json == null || json.isEmpty()) {
            return new UserProfile();
        }
        try {
            return new Gson().fromJson(json, UserProfile.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return new UserProfile();
        }
    }

    public static void clear(Activity context) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PREF_USERPROFILE, "");
        editor.apply();
        instance = new UserProfile();
    }

    public String getCurrentSearch() {
        return currentSearch;
    }

    public void setCurrentSearch(String currentSearch) {
        this.currentSearch = currentSearch;
    }

    public int getSearchType() {
        return searchType;
    }

    public void setSearchType(int searchType) {
        this.searchType = searchType;
    }

    @Override
    public void authorize(String username, String password, boolean rememberMe) {
        this.username = username;
        this.password = password;
        this.rememberMe = rememberMe;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public String getUsername() {
        return username;
    }

    public String getAuthorizationString() {
        String s = username + "@" + COMPANY_ID_DEFAULT + ":" + password;
        String s1 = Base64.encodeToString(s.getBytes(), Base64.NO_WRAP|Base64.URL_SAFE);
//        String s1 = okio.Base64.encode(s.getBytes());
        return "Basic " + s1;
//        return s1;
    }
}
