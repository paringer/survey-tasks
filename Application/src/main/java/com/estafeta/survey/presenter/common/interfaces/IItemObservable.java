package com.estafeta.survey.presenter.common.interfaces;

import com.estafeta.survey.model.SurveyTask;

import rx.Observable;

/**
 * Created by Zhenya on 29.11.2016.
 */
public interface IItemObservable {
    Observable<SurveyTask> getPositionClicks();
}
