/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.estafeta.survey.presenter.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estafeta.survey.model.SurveyTask;
import com.estafeta.survey.presenter.common.interfaces.IItemObservable;
import com.estafeta.survey.view.R;
import com.estafeta.survey.presenter.common.interfaces.IFilterable;

import java.util.ArrayList;
import java.util.Locale;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class TaskItemAdapter extends RecyclerView.Adapter<TaskItemAdapter.ViewHolder> implements IFilterable, IItemObservable {
    private static final String TAG = "TaskItemAdapter";
    public interface SearchType{
        int SEARCH_TYPE_DRIVER = 0;
        int SEARCH_TYPE_SURVEYPOINT = 1;
        int SEARCH_TYPE_CARRIER = 2;
        int SEARCH_TYPE_CAR_MODEL = 3;
        int SEARCH_TYPE_CAR_BRAND = 4;
    }

    private SurveyTask[] mDataSet;
    private SurveyTask[] mBackupDataSet;
    private String search;
    private int searchType =- 1;
    private final PublishSubject<SurveyTask> onClickSubject = PublishSubject.create();

    @Override
    public void filter(String search, int searchType) {
        if(search != null) search = search.toLowerCase(Locale.getDefault());if(search.isEmpty()) mDataSet = mBackupDataSet;
        ArrayList<SurveyTask> filtered = new ArrayList<>();
        for (SurveyTask st : mBackupDataSet) {
            if(st == null) continue;
            switch (searchType){
                case SearchType.SEARCH_TYPE_DRIVER: if(st.Driver != null && st.Driver.toLowerCase(Locale.getDefault()).contains(search)) filtered.add(st); break;
                case SearchType.SEARCH_TYPE_SURVEYPOINT: if(st.SurveyPoint != null && st.SurveyPoint.toLowerCase(Locale.getDefault()).contains(search)) filtered.add(st); break;
                case SearchType.SEARCH_TYPE_CARRIER: if(st.Carrier != null && st.Carrier.toLowerCase(Locale.getDefault()).contains(search)) filtered.add(st); break;
                case SearchType.SEARCH_TYPE_CAR_MODEL: if(st.Model != null && st.Model.toLowerCase(Locale.getDefault()).contains(search)) filtered.add(st); break;
                case SearchType.SEARCH_TYPE_CAR_BRAND: if(st.Brand != null && st.Brand.toLowerCase(Locale.getDefault()).contains(search)) filtered.add(st); break;
                case -1: filtered.add(st); break;
            }
        }
        mDataSet = filtered.toArray(new SurveyTask[0]);
        notifyDataSetChanged();
    }

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView brand;
        private final TextView model;
        private final TextView surveyPoint;
        private final TextView carrier;
        private final TextView driver;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });
            brand = (TextView) v.findViewById(R.id.brandValue);
            model = (TextView) v.findViewById(R.id.modelValue);
            surveyPoint = (TextView) v.findViewById(R.id.surveyPointValue);
            carrier = (TextView) v.findViewById(R.id.carrierValue);
            driver = (TextView) v.findViewById(R.id.driverValue);
        }

        public void bind(SurveyTask task){
            if(task == null) return;
            Log.d(TAG, "bind");
            if(brand != null) brand.setText(task.Brand);
            if(model != null) model.setText(task.Model);
            if(surveyPoint != null) surveyPoint.setText(task.SurveyPoint);
            if(carrier != null) carrier.setText(task.Carrier);
            if(driver != null) driver.setText(task.Driver);
        }

        public TextView getBrand() {
            return brand;
        }

        public TextView getModel() {
            return model;
        }

        public TextView getSurveyPoint() {
            return surveyPoint;
        }

        public TextView getCarrier() {
            return carrier;
        }

        public TextView getDriver() {
            return driver;
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public TaskItemAdapter(SurveyTask[] dataSet) {
        mDataSet = dataSet;
        mBackupDataSet = dataSet;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.task_item, viewGroup, false);

        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        final SurveyTask element = mDataSet[position];
        viewHolder.bind(element);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(element);
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.length;
    }

    @Override
    public Observable<SurveyTask> getPositionClicks(){
        return onClickSubject.asObservable();
    }
}
