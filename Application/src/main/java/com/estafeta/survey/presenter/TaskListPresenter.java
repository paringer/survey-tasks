package com.estafeta.survey.presenter;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.estafeta.survey.model.SurveyTask;
import com.estafeta.survey.model.UserProfile;
import com.estafeta.survey.rest.response.GetTestSurveyTasksResponse;
import com.estafeta.survey.rest.retrofit.IMobileSurveyTasks;
import com.estafeta.survey.view.R;
import com.estafeta.survey.view.interfaces.ITaskListFragment;
import com.estafeta.survey.presenter.common.adapters.TaskItemAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import dagger.Component;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Zhenya on 29.11.2016.
 */
@Component
public class TaskListPresenter {
    private static final String TAG = TaskListPresenter.class.getName();
    public static final String HTTP_AMT1_ESTAFETA_ORG = "http://amt1.estafeta.org/";
    @Nullable
    ITaskListFragment view;

    public TaskListPresenter(ITaskListFragment view) {
        this.view = view;
    }

    public void onDestroyView(){
        view = null;
    }

    public void refreshList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HTTP_AMT1_ESTAFETA_ORG)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IMobileSurveyTasks surveyTasks = retrofit.create(IMobileSurveyTasks.class);
        String authorization = UserProfile.getInstance(view.getActivity()).getAuthorizationString();
        Call<GetTestSurveyTasksResponse> call = surveyTasks.get(authorization);
        call.enqueue(new Callback<GetTestSurveyTasksResponse>() {
        @Override
            public void onResponse(Response<GetTestSurveyTasksResponse> response, Retrofit retrofit) {
                try{
                    if(view!=null) view.setAdapter(new TaskItemAdapter(response.body().toArray(new SurveyTask[0])));
                }catch(Exception ex){
                    if(ex instanceof JsonSyntaxException){
                        showBadAuthMessage("Bad Authentication data");
                    }
                    if(ex!=null) ex.printStackTrace();
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(view != null) view.back();
                        }
                    }, 400);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "what went wrong", t);
                if(t instanceof JsonSyntaxException) {
                    showBadAuthMessage("Bad Authentication data");
                }else{
                    showBadAuthMessage("Some other network error");
                }
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(view != null) view.back();
                    }
                }, 400);
            }
        });
    }

    public void refreshTaskListFromAsset(){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(view == null) return;
                try {
                    SurveyTask[] arr1 = new SurveyTask[0];
                    InputStream stream = view.getActivity().getResources().getAssets().open("sample.json");
                    if(stream == null) return;
                    InputStreamReader reader = new InputStreamReader(stream);
                    SurveyTask[] json = new Gson().fromJson(reader, arr1.getClass());
                    view.setAdapter(new TaskItemAdapter(json));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    private void showBadAuthMessage(String s) {
        if(view!=null) Toast.makeText(view.getActivity(), R.string.signing_in, Toast.LENGTH_LONG).show();
        if(view!=null) new AlertDialog.Builder(view.getActivity(), android.support.v7.appcompat.R.style.Base_Theme_AppCompat_Light_Dialog_Alert).setMessage(s).setTitle("error").setIcon(android.R.drawable.ic_dialog_info).setPositiveButton("Ok",null).show();
    }
}
