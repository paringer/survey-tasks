package com.estafeta.survey.presenter.common.interfaces;

/**
 * Created by Zhenya on 28.11.2016.
 */
public interface IAuthorizable {
    void authorize(String username, String password, boolean rememberMe);
}
