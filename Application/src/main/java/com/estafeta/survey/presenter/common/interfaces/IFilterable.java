package com.estafeta.survey.presenter.common.interfaces;

/**
 * Created by Zhenya on 29.11.2016.
 */
public interface IFilterable {
    void filter(String search, int searchType);
}
